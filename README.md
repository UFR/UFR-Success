A small bot for the discord UFR


Depends on:
```
python3 -m pip install -U https://github.com/Rapptz/discord.py/archive/rewrite.zip
python3 -m pip install json
```

To launch the bot, create a `.key` file containing the API secret key.
