import discord
import asyncio
import os.path
from config import Config


def remove_empty(tab):
    return [t for t in tab if t != '']


async def print_sad(message):
    await message.channel.send(':disappointed_relieved:')
    return 0


async def find_channel(guild):
    """
    Finds a suitable guild channel for posting the
    welcome message.
    """
    for c in guild.text_channels:
        if not c.permissions_for(guild.me).send_messages:
            continue
        return c.id


class MyClient(discord.Client):
    config = Config()

    def get_channels(self):
        default_channel = self.get_channel(id=int(self.config.get('channel')['default']))
        control_channel = self.get_channel(id=int(self.config.get('channel')['control']))
        return default_channel, control_channel



    async def error(self, message, comment):
        default_channel, control_channel = self.get_channels()
        await message.remove_reaction('✅', self.user)
        await message.add_reaction('❌')
        await control_channel.send(comment)
        await print_sad(message)



    async def warning(self, message):
        default_channel, control_channel = self.get_channels()
        await message.remove_reaction('✅', self.user)
        await message.add_reaction('❌')
        await control_channel.send("**WARNING:**\n" + message.author.mention + " tried:\n" + message.content)



    async def guard(self, message):
        default_channel, control_channel = self.get_channels()
        print('author:')
        print(message.author.mention)
        print('list:')
        print(self.config.get('users'))
        if message.channel == control_channel or message.author.mention in self.config.get('users') or message.author.mention[2][3:-1] == '255772441505169408':
            return True
        else:
            await self.warning(message)
            return False



    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)

        if os.path.isfile('config.json'):
            self.config.load()
        else:
            await self.init()
        # default_channel, control_channel = self.get_channels()

        # await control_channel.send('*This is the control channel*')
        # await default_channel.send('*This is the default channel*')

        print('---------------------------------')



    async def init(self):
        self.config = Config()
        for server in self.guilds:
            self.config.get('channel')['default'] = await find_channel(server)
            self.config.get('channel')['control'] = await find_channel(server)
        self.config.store_config()
        default_channel, control_channel = self.get_channels()
        await control_channel.send('This is the control channel')
        await default_channel.send('This is the default channel')



    async def give(self, message):
        """
        give a point to a user with a comment
        :param message: message must be of the form <TYPE> <USER> <OPTIONNAL COMMENT>
        :return:
        """
        default_channel, control_channel = self.get_channels()
        st = message.content.split(" ")
        st = remove_empty(st)

        if len(st) < 3:
            await self.error(message, '*Wrong length.*')
            return

        if not self.config.key_exists(st[1]):
            await self.error(message, '*Wrong key: ' + self.config.pts_str() + '*')
            return

        print(st[2])
        # print(self.user.id)
        if str(st[2][2:-1]) == str(self.user.id):
            await message.channel.send('**C\'est toi le NOOB!** :stuck_out_tongue_closed_eyes: ')
            return

        motif = ''
        if len(st) >= 4:
            motif = " ".join(st[3:])

        # print(self.get_all_members())
        user_lists = [str(member.id) for member in message.guild.members]
        # print(st[2][2:3])
        if st[2][2:3].isdigit():
            user_id = st[2][2:-1]
        else:
            user_id = st[2][3:-1]
        if user_id not in user_lists:
            await self.error(message, '*User does not exist!*')
            st[2] = message.author.mention
            st[1] = 'Boulet'
            motif = 'même pas foutu d\'écrire un nom correctement. :stuck_out_tongue_closed_eyes:'

        if st[2] == message.author.mention:
            st[1] = 'Boulet'
            motif = 'Non, on ne se donne pas des points! :stuck_out_tongue_closed_eyes:'


        f = discord.File("success/" + self.config.get_img(st[1]), filename=self.config.get_img(st[1]))
        if motif == '':
            await default_channel.send('Bravo ' + st[2] + '! Vous avez gagné:')
        else:
            await default_channel.send(
                'Bravo ' + st[2] + '! Vous avez gagné ( pour avoir "' + motif + '" ):')
        await default_channel.send(file=f)

        user_mention_simplified = st[2].replace("!", "")
        if user_mention_simplified in self.config.pts[st[1]]:
            self.config.pts[st[1]][user_mention_simplified] += 1
        else:
            self.config.pts[st[1]][user_mention_simplified] = 1
        self.config.store()

    async def spam(self,message):
        if self.config.config['spam_on_off']:
            for mem in message.guild.members:
                if mem.mention in self.config.get('spam'):
                    if mem.dm_channel == None:
                        await mem.create_dm()
                    await mem.dm_channel.send('Dit ' + mem.mention + ', as-tu lu ce message par ' + message.author.mention + ': ' + message.content)


    def add_list(self,type,candidate):
        if candidate not in self.config.get(type):
         # and candidate != '<@!255772441505169408>':
            self.config.get(type).append(candidate)
            self.config.store()

    def rm_list(self,type,candidate):
        self.config.get(type).remove(candidate)
        self.config.store()


    async def clear(self, message):
        default_channel, control_channel = self.get_channels()
        st = message.content.split(" ")
        st = remove_empty(st)

        if len(st) < 2:
            await self.error(message, '*Wrong length.*')
            return

        user_mention_simplified = st[1].replace("!", "")

        for key in self.config.pts.keys():
            if user_mention_simplified in self.config.pts[key]:
                self.config.pts[key][user_mention_simplified] = 0
        self.config.store()


    async def set(self, message):
        default_channel, control_channel = self.get_channels()
        st = message.content.split(" ")
        st = remove_empty(st)

        if len(st) < 3:
            await self.error(message, '*Wrong length.*')
            return

        if st[1] == 'channel':
            if st[2] == 'default' or st[2] == 'control':
                print(st[3][2:-1])
                self.config.get('channel')[st[2]] = st[3][2:-1]
                await control_channel.send(
                    st[2] + ' channel is now: ' + self.get_channel(id=int(self.config.get('channel')[st[2]])).mention)
                self.config.store()
                return
            else:
                self.error(message, '*Unknown channel type: default or control*')
                return

        if st[1] == 'spam':
            # if st[2] == 'add':
            #     for user_candidate in st[3:]:
            #         self.add_list('spam',user_candidate)
            #     return

            if st[2] == 'rm':
                for user_candidate in st[3:]:
                    self.rm_list('spam',user_candidate)
                return

            if st[2] == 'on':
                self.config.config['spam_on_off'] = True
                self.config.store()
                return

            if st[2] == 'off':
                self.config.config['spam_on_off'] = False
                self.config.store()
                return

        if st[1] == 'spam_trigger':
            if st[2] == 'add':
                for candidate in st[3:]:
                    self.add_list('spam_trigger',candidate)
                return

            if st[2] == 'rm':
                for candidate in st[3:]:
                    self.rm_list('spam_trigger',candidate)
                return

        if st[1] == 'spam_untrigger':
            if st[2] == 'add':
                for candidate in st[3:]:
                    self.add_list('spam_untrigger',candidate)
                return

            if st[2] == 'rm':
                for user_candidate in st[3:]:
                    self.rm_list('spam_untrigger',user_candidate)
                return

        if not self.config.key_exists(st[1]):
            self.error(message, '*Wrong key: ' + self.config.pts_str() + '*')
            return

        if not st[3].isdigit():
            self.error(message, '*Not a digit.*')
            return

        self.config.pts[st[1]][st[2]] = int(st[3])
        self.config.store()

    async def users_change(self, message):
        default_channel, control_channel = self.get_channels()
        st = message.content.split(" ")
        st = remove_empty(st)

        if len(st) < 2:
            self.error(message, '*Wrong length.*')
            return
        if st[1] == 'list':
            await control_channel.send('Users in control:\n' + "\n".join(self.config.get('users')))
            return

        if len(st) < 3:
            self.error(message, '*Wrong length.*')
            return

        if st[1] == 'add':
            for user_candidate in st[2:]:
                if user_candidate not in self.config.get('users'):
                    self.config.get('users').append(user_candidate)
                    await control_channel.send(user_candidate + ' can now control the bot.')
                else:
                    await control_channel.send(user_candidate + ' could already control the bot.')
            self.config.store()
            return

        if st[1] == 'rm':
            self.config.get('users').remove(st[2])
            await control_channel.send(st[2] + ' can no longer control the bot.')
            self.config.store()
            return

        self.error(message, 'I did not understand.')



    async def stats(self):
        default_channel, control_channel = self.get_channels()
        description = self.config.print_points()
        if len(description) == 0:
            embed = discord.Embed(title="UFR-Success", description="Pas de points à afficher.", color=0xeee657)
            await default_channel.send(embed=embed)
        else:
            for desc in description:
                embed = discord.Embed(title="UFR-Success", description=desc, color=0xeee657)
                await default_channel.send(embed=embed)


    async def trigger(self,message):
        st = message.content.split(" ")
        st = remove_empty(st)
        for sti in st:
            if sti in self.config.get('spam_trigger'):
                self.add_list('spam',message.author.mention)
                await message.add_reaction('✅')
                return
        for sti in st:
            if sti in self.config.get('spam_untrigger'):
                if message.author.mention in self.config.get('spam'):
                    self.rm_list('spam',message.author.mention)
                    await message.add_reaction('✅')
                return


    async def on_message(self, message):
        # Default_channel, control_channel = self.get_channels()

        # don't respond to ourselves
        if message.author == self.user:
            return

        if message.content.startswith('!'):
            await message.add_reaction('✅')
            st = message.content.split(" ")
            st = remove_empty(st)

            keyword = st[0][1:]  # get rid of !
            if keyword == 'greet':
                await message.channel.send(':smiley: :wave: Hello, there!')

            elif keyword == 'info':
                embed = discord.Embed(title="UFR-Success", description="Je compte le nombre de cheveux de Gizon.",
                                      color=0xeee657)
                embed.add_field(name="Author", value="ildyria")  # give info about you here
                await message.channel.send(embed=embed)

            elif keyword == 'fail':
                await message.channel.send("**C'est toi le NOOB!** :stuck_out_tongue_closed_eyes: ")

            elif keyword == 'stats':
                await self.stats()

            elif keyword == 'give':
                if await self.guard(message):
                    await self.give(message)

            elif keyword == 'set':
                if await self.guard(message):
                    await self.set(message)

            elif keyword == 'clear':
                if await self.guard(message):
                    await self.clear(message)

            elif keyword == 'users':
                if await self.guard(message):
                    await self.users_change(message)

            elif keyword == 'reload':
                if await self.guard(message):
                    await self.config.load()

            elif keyword == 'reset':
                if await self.guard(message):
                    if message.author.mention[2][3:-1] == '255772441505169408':
                        await self.init()
                    else:
                        await message.channel.send('Sorry but this function is only available to <@255772441505169408> !')

            elif keyword == 'help':

                description = '**available commands:** \n'
                description += '!info\n   basic info\n'
                description += '!stats\n   prints stats\n'
                description += '!greet\n   say Hi!\n'
                description += '!fail\n'

                if await self.guard(message):
                    description += '\n'
                    description += '**Admin commands:** \n'
                    description += '!reload\n'
                    description += '   Reload the success (useful if a success has been added)\n'
                    description += '\n'
                    description += '!give <type> @blabla [optional comment]\n'
                    description += '   where <type> is ' + ", ".join(list(self.config.pts.keys())) + '\n'
                    description += '\n'
                    description += '!users [add|rm|list] [optional @blabla] \n'
                    description += '   list users authorized to give Success.\n'
                    description += '\n'
                    description += '!set <type> @blabla `num`\n'
                    description += '    set the number of <type> for @blabla to `num`\n'
                    description += '\n'
                    description += '!clear <123456789>\n'
                    description += '    remove all the points of <123456789> (useful to clean number in stats when someone is kicked out of the clan)\n'
                    description += '\n'
                    description += '!set channel <type> `#channel-name`\n'
                    description += '    set the <type> channel to `#channel-name`\n'
                embed = discord.Embed(title="UFR-Success", description=description, color=0xeee657)
                await message.channel.send(embed=embed)

            else:
                await self.error(message, 'I did not understand.')
        else:
            default_channel, control_channel = self.get_channels()
            if message.channel.id == control_channel.id:
                pass
            else:
                await self.trigger(message)
                await self.spam(message)


if __name__ == "__main__":
    with open('.key', 'r', encoding="utf-8") as file:
        cred_data = file.read().split('\n')
        # print(cred_data[0])
        client = MyClient()
        client.run(cred_data[0])

# print(message.author)
# print(message.author.mention)
# print(message.author.id)
# print(self.config.users)

# id_user = st[2]
# user = discord.utils.get(message.channel.members, name = 'ildyria', discriminator = 7128)
# await message.channel.send(id_user)

# give users a link to invite this bot to their server embed.add_field(name="Invite", value="[Invite link](
# https://discordapp.com/api/oauth2/authorize?client_id=456847303479787520&permissions=256064&scope=bot)")
