import json
import os.path
import operator


def print_dic(dic):
    for keys, values in dic.items():
        print(keys + ' => ' + values)


def print_array(dic):
    for keys in dic:
        print(keys)


class Config:
    # 'Gizon': 'PGizon.png',
    # 'BB': 'PBB.png',
    # 'CV': 'PCV.png',
    # 'CA': 'PCA.png',
    # 'DD': 'PDD.png',
    # 'Silence': 'PSilence.png',
    # 'Caddil': 'PCaddil.png',
    # 'Deto': 'PDeto.png',
    # 'Burgonde': 'PBurgonde.png',

    # this is now determined by the files in success: easier to add success now.
    mapping_pts_image = {}
    pts = {}
    config = {
        'users': [],
        'channel': {},
        'spam': [],
        'spam_trigger': [],
        'spam_untrigger': [],
        'spam_on_off': True
    }

    def __init__(self):
        self.load_success(False)
        for key in self.mapping_pts_image.keys():
            if key not in self.pts:
                self.pts[key] = {}

    def get(self, key):
        return self.config[key]

    def get_img(self, key):
        return self.mapping_pts_image[key]

    def pts_str(self):
        return ", ".join(self.mapping_pts_image.keys())

    def store_config(self):
        with open('config.json', 'w') as file_save:
            json.dump(self.config, file_save)

    def store_pts(self):
        with open('pts.json', 'w') as file_save:
            json.dump(self.pts, file_save)

    def store(self):
        self.store_config()
        self.store_pts()

    def load_success(self, printing=True):
        successes = os.listdir('success')
        # print(successes)
        if printing:
            print('---------------------------------')
            print('Possible successes:')
        for success in successes:
            self.mapping_pts_image[success[1:-4]] = success
        if printing:
            print_dic(self.mapping_pts_image)

    def load_pts(self):
        with open('pts.json') as data_file:
            self.pts = json.load(data_file)

        # we need to check if pts is actually as it should (key matches)
        for key in self.mapping_pts_image.keys():
            if key not in self.pts:
                self.pts[key] = {}
                print("new key found: " + key)

    def load_config(self):
        with open('config.json') as data_file:
            config = json.load(data_file)
            for key in config.keys():
                if key in self.config.keys():
                    self.config[key] = config[key]
                else:
                    print(key + 'is config less:' + config[key])

    def load(self):
        self.load_success()
        self.load_pts()
        self.load_config()
        self.compact()
        # self.store()

    def print_points(self):
        self.purge()
        list_description = []
        for key in self.mapping_pts_image.keys():
            value = self.pts[key]
            if len(value) > 0:
                description = '**' + key + '**\n'
                # for keys, values in value.items():
                for keys, values in sorted(value.items(), key=operator.itemgetter(1), reverse=True):
                    description += keys + ' : ' + str(values) + '\n'
                list_description.append(description)
        return list_description

    def purge(self):
        for key in list(self.mapping_pts_image.keys()):
            if len(self.pts[key]) > 0:
                for keys in list(self.pts[key].keys()):
                    if self.pts[key][keys] == 0:
                        del self.pts[key][keys]
        print(self.pts)
        self.store()

    def compact(self):
        for key in list(self.mapping_pts_image.keys()):
            if len(self.pts[key]) > 0:
                for keys in list(self.pts[key].keys()):
                    if keys[2] == '!':
                        newkey = keys.replace("!", "")
                        if newkey in self.pts[key]:
                            v = self.pts[key][newkey]
                            self.pts[key][newkey] = v + self.pts[key][keys]
                        else:
                            self.pts[key][newkey] = self.pts[key][keys]
                        del self.pts[key][keys]
        print(self.pts)
        self.store()


    def key_exists(self, value):
        return value in self.pts.keys()
